PROJECT = olifer

DEPS = jsx
TEST_DEPS = LIVR

dep_jsx       = git git@git.ceb.loc:erlang/jsx.git        v2.6.2
dep_LIVR      = git git@git.ceb.loc:erlang/LIVR.git       master

TEST_DIR = test

include erlang.mk
