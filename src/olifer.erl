-module(olifer).

-include("olifer.hrl").

%% LIVR API
-export([validate/2]).
-export([validate/3]).
-export([register_rule/3]).
-export([register_aliased_rule/1]).
%% FOR INTERNAL USAGE
-export([start/0, stop/0]).
-export([validate_data/2]).
-export([validate_data/3]).
-export([get_value/2]).
-export([get_value/3]).


-export([test/1, mask_val/3]).

-define(TCardL, [
    [
        {<<"id">>, <<"0">>},
        {<<"number">>,<<"26302626895856">>},
        {<<"expd">>,<<"1111">>}
    ],
    [
        {<<"id">>, <<"1">>},
        {<<"number">>,<<"1212121212121212">>},
        {<<"expd">>,<<"1111">>}
    ],
    [
        {<<"id">>, <<"2">>},
        {<<"number">>,<<"1313131313131313">>},
        {<<"expd">>,<<"1111">>}
    ]
]).

%% LIVR API
-spec validate(Data :: [{binary(), any()}], Rules :: [{binary(), any()}]) ->
    {ok, Data2 :: [{binary(), any()}]} | {errors, [any()]}.
validate(Data, Rules) ->
    validate(Data, Rules, []).

-spec validate(Data :: [{binary(), any()}], Rules :: [{binary(), any()}], [any()]) ->
    {ok, Data2 :: [{binary(), any()}]} | {errors, [any()]}.
validate([], [], _) ->
    {ok, [{}]};
validate([{}], [], _) ->
    {ok, [{}]};
validate(_Data, [{}], _) ->
    {ok, [{}]};
validate(Data, Rules, Args) when is_list(Data), is_list(Rules) ->
    FieldsList = validate_data(Data, Rules, Args),
    {Type, Result} = return_result(FieldsList, [], []),
    {Type, lists:reverse(Result)};
validate(Data, _Rules, _Args) ->
    [{Data, ?FORMAT_ERROR}].

-spec register_rule(binary(), module(), atom()) -> ok.
register_rule(Name, Module, Function) when is_atom(Module), is_atom(Function) ->
    true = ets:insert(?RULES_TBL, {Name, Module, Function}),
    ok.

-spec register_aliased_rule(list()) -> ok.
register_aliased_rule(Aliases) when is_list(Aliases) ->
    register_aliases(Aliases).

%% FOR INTERNAL USAGE
get_value(Key, List) when is_list(List) ->
    get_value(Key, List, undefined);
get_value(_, _) ->
    undefined.

get_value(Key, List, Default) ->
    case lists:keyfind(Key, 1, List) of
        {_, Val} -> Val;
        _ -> Default
    end.

start() ->
    start(?MODULE).

stop() ->
    application:stop(?MODULE).

-spec validate_data(maybe_improper_list(), maybe_improper_list()) -> [#field{}].
validate_data(DataPropList, RulesPropList) when is_list(DataPropList), is_list(RulesPropList) ->
    validate_data(DataPropList, RulesPropList, []).

-spec validate_data(maybe_improper_list(), maybe_improper_list(), PrivArgs :: [any()]) -> [#field{}].
validate_data(DataPropList, RulesPropList, PrivArgs) when is_list(DataPropList), is_list(RulesPropList) ->
	  StrictMode = olifer:get_value(strict_mode, PrivArgs, true),
	  ListOfFields = prepare(StrictMode, DataPropList, RulesPropList),
    lists:reverse([apply_rules(StrictMode, Field, PrivArgs, DataPropList) || Field <- ListOfFields]);
validate_data(Data, Rules, _PrivArgs) ->
    [#field{name = Data, input = Data, rules = Rules, output = ?FORMAT_ERROR, errors = ?FORMAT_ERROR}].

%% INTERNAL
return_result([], AccOk, []) ->
    {ok, AccOk};
return_result([], _, AccErr) ->
    {errors, AccErr};
return_result([#field{errors = [], output = remove_obj} = _Field|Rest], AccOk, AccErr) ->
    return_result(Rest, AccOk, AccErr);
return_result([#field{errors = []} = Field|Rest], AccOk, AccErr) ->
    return_result(Rest, [{Field#field.name, Field#field.output}|AccOk], AccErr);
return_result([Field|Rest], AccOk, AccErr) ->
    return_result(Rest, AccOk, [{Field#field.name, Field#field.errors}|AccErr]).

register_aliases([]) ->
    ok;
register_aliases([Alias | Rest]) ->
    Name = olifer:get_value(<<"name">>, Alias),
    Rules = olifer:get_value(<<"rules">>, Alias),
    ErrorCode = olifer:get_value(<<"error">>, Alias),
    true = ets:insert(?ALIASES_TBL, {Name, Rules, ErrorCode}),
    register_aliases(Rest).

lookup_alias(Name) ->
    case ets:lookup(?ALIASES_TBL, Name) of
        [] -> undefined;
        [{Name, Rules, Error} | _] -> {Name, Rules, Error}
    end.

lookup_rules(Name) ->
    case ets:lookup(?RULES_TBL, Name) of
        [] -> undefined;
        [{Name, Module, Function} | _] -> {Name, Module, Function}
    end.

prepare(StrictMode, DataPropList, RulesPropList) ->
    prepare(StrictMode, DataPropList, DataPropList, RulesPropList, []).

prepare(true, _DataPropList, _AllData, [], Acc) ->
    Acc;
%% aren't rules
prepare(false, DataPropList, _AllData, [], Acc) when DataPropList =/= [] ->
    Fun = fun({K, V}, DeppAcc) ->
            case lists:keymember(K, 2, Acc) of
                true ->
                    DeppAcc;
                false ->
                    [#field{name = K, input= V, rules = [<<"oke">>]}|DeppAcc]
            end
          end,
    Acc ++ lists:foldr(Fun, [], DataPropList);
prepare(false, _DataPropList, _AllData, [], Acc) ->
    Acc;
prepare(_StrictMode, _DataPropList, _AllData, [{}], Acc) ->
    Acc;
%% there are rules
prepare(StrictMode, DataPropList, AllData, [{FieldName, FieldRules} | RestRules], Acc) ->
    case lists:keyfind(FieldName, 1, DataPropList) of
        false ->
            case has_spec_rule(FieldRules) of
                false ->
                    prepare(StrictMode, DataPropList, AllData, RestRules, Acc);
                SpecRulesList ->
                    prepare(StrictMode, DataPropList, AllData, RestRules, [#field{name = FieldName, input = <<>>, rules = SpecRulesList}|Acc])
            end;
        {FieldName, FieldData} ->
            prepare(StrictMode, DataPropList, AllData, RestRules, [#field{name = FieldName, input = FieldData, rules = FieldRules}|Acc])
    end.

apply_rules(_StrictMode, #field{rules = []} = Field, _PrivArgs, _AllData) ->
    Field;
apply_rules(StrictMode, #field{rules = {Rule, Args}} = Field, PrivArgs, AllData) ->
    {NewInp, Output, Errors} = apply_one_rule(StrictMode, Field, {Rule, Args}, PrivArgs, AllData),
    apply_rules(StrictMode, Field#field{input = NewInp, rules = [], output = Output, errors = Errors}, PrivArgs, AllData);
apply_rules(StrictMode, #field{rules = Rule} = Field, PrivArgs, AllData) when is_binary(Rule) ->
    {NewInp, Output, Errors} = apply_one_rule(StrictMode, Field, {Rule, []}, PrivArgs, AllData),
    apply_rules(StrictMode, Field#field{input = NewInp, rules = [], output = Output, errors = Errors}, PrivArgs, AllData);
apply_rules(StrictMode, #field{rules = [Rule | Rest]} = Field, PrivArgs, AllData) ->
    case apply_one_rule(StrictMode, Field, Rule, PrivArgs, AllData) of
        {NewInp, Output, []} ->
            apply_rules(StrictMode, Field#field{rules = Rest, input = NewInp, output = Output, errors = []}, PrivArgs, AllData);
        {_, Error, Error} ->
            apply_rules(StrictMode, Field#field{rules = [], output = Error, errors = Error}, PrivArgs, AllData)
    end.

apply_one_rule(StrictMode, Field, Rule, PrivArgs, AllData) when is_binary(Rule) ->
    apply_one_rule(StrictMode, Field, Rule, [], PrivArgs, AllData);
apply_one_rule(StrictMode, Field, [{Rule, Args}], PrivArgs, AllData) ->
    apply_one_rule(StrictMode, Field, Rule, Args, PrivArgs, AllData);
apply_one_rule(StrictMode, Field, {Rule, Args}, PrivArgs, AllData) ->
    apply_one_rule(StrictMode, Field, Rule, Args, PrivArgs, AllData).

apply_one_rule(StrictMode, Field, Rule, Args, PrivArgs, AllData) ->
    case rule_to_atom(Rule) of
        undefined ->
            process_result(apply_user_rules(StrictMode, Field, Rule, Args, PrivArgs, AllData), Field#field.input);
        {Module, Function} ->
            process_result(erlang:apply(Module, Function, [Field#field.input, Args, AllData, PrivArgs]), Field#field.input)

    end.

apply_user_rules(StrictMode, Field, Rule, Args, PrivateArgs, AllData) ->
    case apply_aliased_rules(StrictMode, Field, Rule, PrivateArgs, AllData) of
        undefined -> apply_registered_rules(StrictMode, Field, Rule, Args, PrivateArgs);
        Res -> Res
    end.

apply_aliased_rules(StrictMode, Field, RuleName, PrivArgs, AllData) ->
    case lookup_alias(RuleName) of
        undefined ->
            undefined;
        {RuleName, Rules, undefined} ->
            case apply_rules(StrictMode, Field#field{rules = Rules, errors = []}, PrivArgs, AllData) of
                #field{errors = []} = NewField -> {ok, NewField#field.output};
                NewField -> {error, NewField#field.errors}
            end;
        {RuleName, Rules, Error} ->
            case apply_rules(StrictMode, Field#field{rules = Rules, errors = []}, PrivArgs, AllData) of
                #field{errors = []} = NewField -> {ok, NewField#field.output};
                _ -> {error, Error}
            end
    end.

apply_registered_rules(_StrictMode, Field, RuleName, Args, PrivArgs) ->
    case lookup_rules(RuleName) of
        undefined ->
            {undefined, RuleName};
        {RuleName, Module, Function} ->
            erlang:apply(Module, Function, [Field#field.input, Args, PrivArgs])
    end.

process_result({undefined, RuleName}, Input) ->
    {Input, Input, <<"Rule '", RuleName/binary, "' not_registered">>};
process_result({filter, Output}, _) ->
    {Output, Output, []};
process_result({ok, Output}, Input) ->
    {Input, Output, []};
process_result({error, Error}, Input) ->
    {Input, Error, Error};
process_result({remove_obj, Output}, _Input) ->
    {Output, remove_obj, []};
process_result(Unexpected, Input) ->
    ct:print("Unexpected: ~p~nInput:~p~n", [Unexpected, Input]).


rule_to_atom(<<"required">>) -> {olifer_commons, required};
rule_to_atom(<<"not_empty">>) -> {olifer_commons, not_empty};
rule_to_atom(<<"not_empty_list">>) -> {olifer_commons, not_empty_list};
rule_to_atom(<<"any_object">>) -> {olifer_commons, any_object};
rule_to_atom(<<"oke">>) -> {olifer_commons, oke};
rule_to_atom(<<"boolean">>) -> {olifer_commons, boolean};

rule_to_atom(<<"integer">>) -> {olifer_numeric, integer};
rule_to_atom(<<"positive_integer">>) -> {olifer_numeric, positive_integer};
rule_to_atom(<<"decimal">>) -> {olifer_numeric, decimal};
rule_to_atom(<<"positive_decimal">>) -> {olifer_numeric, positive_decimal};
rule_to_atom(<<"max_number">>) -> {olifer_numeric, max_number};
rule_to_atom(<<"min_number">>) -> {olifer_numeric, min_number};
rule_to_atom(<<"number_between">>) -> {olifer_numeric, number_between};

rule_to_atom(<<"one_of">>) -> {olifer_string, one_of};
rule_to_atom(<<"max_length">>) -> {olifer_string, max_length};
rule_to_atom(<<"min_length">>) -> {olifer_string, min_length};
rule_to_atom(<<"length_between">>) -> {olifer_string, length_between};
rule_to_atom(<<"length_equal">>) -> {olifer_string, length_equal};
rule_to_atom(<<"like">>) -> {olifer_string, like};
rule_to_atom(<<"string">>) -> {olifer_string, string};
rule_to_atom(<<"eq">>) -> {olifer_string, eq};

rule_to_atom(<<"email">>) -> {olifer_special, email};
rule_to_atom(<<"url">>) -> {olifer_special, url};
rule_to_atom(<<"iso_date">>) -> {olifer_special, iso_date};
rule_to_atom(<<"equal_to_field">>) -> {olifer_special, equal_to_field};

rule_to_atom(<<"nested_object">>) -> {olifer_metarules, nested_object};
rule_to_atom(<<"list_of">>) -> {olifer_metarules, list_of};
rule_to_atom(<<"list_of_objects">>) -> {olifer_metarules, list_of_objects};
rule_to_atom(<<"list_of_different_objects">>) -> {olifer_metarules, list_of_different_objects};
rule_to_atom(<<"variable_object">>) -> {olifer_metarules, variable_object};
rule_to_atom(<<"or">>) -> {olifer_metarules, 'or'};

rule_to_atom(<<"trim">>) -> {olifer_modifiers, trim};
rule_to_atom(<<"to_lc">>) -> {olifer_modifiers, to_lc};
rule_to_atom(<<"to_uc">>) -> {olifer_modifiers, to_uc};
rule_to_atom(<<"remove">>) -> {olifer_modifiers, remove};
rule_to_atom(<<"remove_val">>) -> {olifer_modifiers, remove_val};
rule_to_atom(<<"leave_only">>) -> {olifer_modifiers, leave_only};
rule_to_atom(<<"default">>) -> {olifer_modifiers, default};

rule_to_atom(_) -> undefined.

has_spec_rule([FieldRules]) when is_list(FieldRules) ->
    has_spec_rule(FieldRules);
has_spec_rule(FieldRules) ->
    has_spec_rule(FieldRules, ?SPEC_RULES, []).

has_spec_rule(_FieldRules, [], []) ->
    false;
has_spec_rule(_FieldRules, [], Acc) ->
    Acc;
has_spec_rule(FieldRules, [Type | RestTypes], Acc) ->
    case has_rule(FieldRules, Type) of
        {true, FieldRule} -> has_spec_rule(FieldRules, RestTypes, [FieldRule|Acc]);
        _ -> has_spec_rule(FieldRules, RestTypes, Acc)
    end.

has_rule(Type, Type) ->
    {true, Type};
has_rule([], _Type) ->
    false;
has_rule([Type | _Rest], Type) ->
    {true, Type};
has_rule([{Type, _} = Rule| _Rest], Type) ->
    {true, Rule};
has_rule([[{Type, _} = Rule] | _Rest], Type) ->
    {true, Rule};
has_rule([_ | Rest], Type) ->
    has_rule(Rest, Type);
has_rule(_, _) ->
    false.

start(AppName) ->
    F = fun({App, _, _}) -> App end,
    RunningApps = lists:map(F, application:which_applications()),
    ok = load(AppName),
    {ok, Dependencies} = application:get_key(AppName, applications),
    [begin
         ok = start(A)
     end || A <- Dependencies, not lists:member(A, RunningApps)],
    ok = application:start(AppName).

load(AppName) ->
    F = fun({App, _, _}) -> App end,
    LoadedApps = lists:map(F, application:loaded_applications()),
    case lists:member(AppName, LoadedApps) of
        true ->
            ok;
        false ->
            ok = application:load(AppName)
    end.

%%=======
%% Quik Tests
%%=======
%% INTERNALS
mask_val(<<>> = Value, _Args, _) when is_binary(Value) ->
    {ok, Value};
mask_val(Value, RuleArgs, _) when is_binary(Value) ->
    ModVal = parse_trans_val(RuleArgs, Value),
    {ok, parse_trans_val(RuleArgs, ModVal)};
mask_val(Value, RuleArgs, _) when is_integer(Value) ->
    {ok, parse_trans_val(RuleArgs, to_binary(Value))}.

parse_trans_val([], ModValue) -> ModValue;
parse_trans_val([{MaskElem, [From, To|_]}|T], ModValue)
    when is_integer(From) andalso is_integer(To) andalso is_binary(ModValue)
    andalso byte_size(ModValue) >= To ->
    {RawData, LastView} = erlang:split_binary(ModValue, To),
    {FirstView, NeedMask} = erlang:split_binary(RawData, From-1),
    CopiedMask = binary:copy(MaskElem, byte_size(NeedMask)),
    parse_trans_val(T, <<FirstView/binary, CopiedMask/binary, LastView/binary>>);
parse_trans_val([_InvalidRule|T], ModValue) ->
    parse_trans_val(T, ModValue).

to_binary(null) -> <<>>;
to_binary(X) when is_atom(X) ->
    erlang:atom_to_binary(X, utf8);
to_binary(X) when is_list(X) ->
    list_to_binary(X);
to_binary(X) when is_reference(X) ->
    to_binary(erlang:ref_to_list(X));
to_binary(X) when is_pid(X) ->
    list_to_binary(pid_to_list(X));
to_binary(X) when is_integer(X) ->
    erlang:integer_to_binary(X);
to_binary(X) when is_binary(X) -> X.

%% Functional tests
%% olifer:test(1).
test(1) ->
    Rules = [   {<<"action">>, [<<"required">>, {<<"max_length">>, 50}]},
        {<<"templateParams">>, [
            {<<"default">>, [{}]},
            {<<"list_of_objects">>, [
                {<<"name">>, [<<"required">>, {<<"max_length">>,50}]},
                {<<"value">>, [<<"required">>, {<<"max_length">>, 300}]}
            ]}
        ]}
    ],
    Input = [{<<"action">>, <<"get">>}],
    olifer:validate(Input, Rules);

%% olifer:test(2).
test(2) ->
    ok = olifer:register_rule(<<"mask_val">>, ?MODULE, mask_val),
    Input = [{<<"card_l">>, [
        [
            {<<"id">>, <<"0">>},
            {<<"hren">>, <<"yoyo">>},
            {<<"number">>,<<"26302626895856">>},
            {<<"expd">>,<<"1111">>}
        ]
    ]}],
    Rules = [{<<"card_l">>, [
        {<<"list_of_objects">>, [
            {<<"hren">>,  [{<<"mask_val">>, [{<<"*">>,[2,4]}]}]},
            {<<"number">>,  [{<<"mask_val">>, [{<<"*">>,[3,5]}]}]},
            {<<"expd">>, <<"remove_val">>}
        ]}]}],
    olifer:validate(Input, Rules, [<<"hren">>, {strict_mode, false}]);

%% olifer:test(3).
test(3) ->
    ok = olifer:register_rule(<<"mask_val">>, ?MODULE, mask_val),
    Input = [{<<"$callbackData">>, [{<<"data">>, ?TCardL}]}],
    %% Rule
    Rules = [{<<"$callbackData">>, [
        {<<"nested_object">>,[
            {<<"data">>, [
                {<<"list_of_objects">>, [
                    {<<"number">>, [{<<"mask_val">>, [{<<"*">>, [3, 5]}]}]},
                    {<<"expd">>, [{<<"mask_val">>, [{<<"*">>, [1, 4]}]}]}
                ]}
            ]}
        ]}
    ]}],
    olifer:validate(Input, Rules, [<<"hren">>, {strict_mode, false}]);

%% olifer:test(4).
test(4) ->
    Rules = [{<<"options">>, <<"remove_val">>}],
    Input = [
        {<<"type">>,<<"p2p">>},
        {<<"amount">>,<<"2">>},
        {<<"currency">>,<<"UAH">>},
        {<<"cardA">>,<<"1111111111111111">>},
        {<<"expDateA">>,<<"0818">>},
        {<<"details">>,<<"ererer">>},
        {<<"cardB">>,<<"1111111111111111">>},
        {<<"nameB">>,<<>>},
        {<<"options">>,<<"test_opts">>},
        {<<"id">>,<<"1674511.14615985039920.23853770014829934">>}
    ],
    olifer:validate(Input, Rules, [<<"test_args">>, {strict_mode, false}]);

%% olifer:test(5).
test(5) ->
    Input = [{<<"test_bool1">>, <<"true">>},{<<"test_bool2">>, false}],
    Rules = [{<<"test_bool1">>, <<"boolean">>}, {<<"test_bool2">>, <<"boolean">>}],
    olifer:validate(Input, Rules, []).