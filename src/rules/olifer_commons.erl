-module(olifer_commons).
-author("prots.igor@gmail.com").

-include("olifer.hrl").

%% API
-export([
    oke/4,
    required/4,
    not_empty/4,
    not_empty_list/4,
    any_object/4,
    boolean/4
]).

%% API
%% for filter
-spec oke(binary(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
oke(Value, _, _, _) ->
    {ok, Value}.

-spec boolean(binary() | boolean(), list(), [any()], [any()]) ->
    {ok, true | false} | {error, binary()}.
boolean(ValueTrue, _RuleArgs, _SessData, _PrivArgs)
    when ValueTrue =:= <<"true">> orelse ValueTrue =:= true ->
    {ok, true};
boolean(ValueFalse, _RuleArgs, _SessData, _PrivArgs)
    when ValueFalse =:= <<"false">> orelse ValueFalse =:= false ->
    {ok, false};
boolean(_Any, _RuleArgs, _SessData, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec required(null | undefined | binary(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
required(<<>>, [], _, _PrivArgs) ->
    {error, ?REQUIRED};
required(null, [], _, _PrivArgs) ->
    {error, ?REQUIRED};
required(undefined, [], _, _PrivArgs) ->
    {error, ?REQUIRED};
required(Value, [], _, _PrivArgs) ->
    {ok, Value}.

-spec not_empty(binary(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
not_empty(<<>>, [], _, _PrivArgs) ->
    {error, ?CANNOT_BE_EMPTY};
not_empty(Value, [], _, _PrivArgs) ->
    {ok, Value}.

-spec not_empty_list(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
not_empty_list(<<>>, _Args, _, _PrivArgs) ->
    {error, ?CANNOT_BE_EMPTY};
not_empty_list([], _Args, _, _PrivArgs) ->
    {error, ?CANNOT_BE_EMPTY};
not_empty_list([{}], [], _, _PrivArgs) ->
    {error, ?WRONG_FORMAT};
not_empty_list(Value, [], _, _PrivArgs) when is_list(Value) ->
    {ok, Value};
not_empty_list(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec any_object(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
any_object(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
any_object([{}] = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
any_object([], _Args, _PrivArgs, _PrivArgs) ->
    {error, ?FORMAT_ERROR};
any_object(Value, _Args, _, _PrivArgs) when is_list(Value)->
    any_object_impl(Value, Value);
any_object(Value, _Args, _, _PrivArgs) when is_tuple(Value) ->
    {ok, Value};
any_object(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

any_object_impl([], Value) ->
    {ok, Value};
any_object_impl([Object|Rest], Value) when is_tuple(Object) ->
    any_object_impl(Rest, Value);
any_object_impl([_|_], _) ->
    {error, ?FORMAT_ERROR}.
