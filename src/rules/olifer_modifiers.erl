-module(olifer_modifiers).
-author("prots.igor@gmail.com").

-include("olifer.hrl").

%% API
-export([
    trim/4,
    to_uc/4,
    to_lc/4,
    remove/4,
    remove_val/4,
    leave_only/4,
    default/4
]).

%% API
-spec trim(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
trim(Value, [], _, _PrivArgs) when is_binary(Value) ->
    {filter, bstring:trim(Value)};
trim(Value, [], _, _PrivArgs) ->
    {filter, Value}.

-spec to_lc(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
to_lc(Value, [], _, _PrivArgs) when is_binary(Value) ->
    {filter, bstring:to_lower(Value)};
to_lc(Value, [], _, _PrivArgs) ->
    {filter, Value}.

-spec to_uc(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
to_uc(Value, [], _, _PrivArgs) when is_binary(Value) ->
    {filter, bstring:to_upper(Value)};
to_uc(Value, [], _, _PrivArgs) ->
    {filter, Value}.

-spec remove(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
remove(Value, [Pattern], AllData, _PrivArgs) ->
    remove(Value, Pattern, AllData, _PrivArgs);
remove(Value, Pattern, _, _PrivArgs) when is_binary(Value), is_binary(Pattern) ->
    remove_impl(Value, Pattern);
remove(Value, _, _, _PrivArgs) ->
    {filter, Value}.

-spec remove_val(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
remove_val(_ValueTrue, _RuleArgs, _SessData, _PrivArgs) ->
    {remove_obj, []}.

-spec leave_only(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
leave_only(Value, [Pattern], AllData, _PrivArgs) ->
    leave_only(Value, Pattern, AllData, _PrivArgs);
leave_only(Value, Pattern, _, _PrivArgs)  when is_binary(Value), is_binary(Pattern)->
    leave_only_impl(Value, Pattern, <<>>);
leave_only(Value, _, _, _PrivArgs) ->
    {filter, Value}.

-spec default(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
default(<<>>, [[]], _AllData, _PrivArgs) ->
    {filter, []};
default(<<>>, [{}], _AllData, _PrivArgs) ->
    {filter, [{}]};
default(<<>>, [Default], _AllData, _PrivArgs) ->
    {filter, Default};
default(<<>>, Default, _AllData, _PrivArgs) ->
    {filter, Default};
default(Value, _Default, _AllData, _PrivArgs) ->
    {filter, Value}.

%% INTERNAL
remove_impl(Value, <<>>) ->
    {filter, Value};
remove_impl(Value, <<First:1/binary, Rest/binary>>) ->
    NewValueList = bstring:split_global(Value, First),
    remove_impl(binary:list_to_bin(NewValueList), Rest).

leave_only_impl(<<>>, _, Acc) ->
    {filter, Acc};
leave_only_impl(<<First:1/binary, Rest/binary>>, Pattern, Acc) ->
    case binary:match(Pattern, First) of
        nomatch -> leave_only_impl(Rest, Pattern, Acc);
        _ -> leave_only_impl(Rest, Pattern, <<Acc/binary, First/binary>>)
    end.
