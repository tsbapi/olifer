-module(olifer_numeric).
-author("prots.igor@gmail.com").

-include("olifer.hrl").

%% API
-export([integer/4]).
-export([positive_integer/4]).
-export([decimal/4]).
-export([positive_decimal/4]).
-export([max_number/4]).
-export([min_number/4]).
-export([number_between/4]).

%% API
-spec integer(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
integer(<<>> = Value, [], _, _PrivArgs) ->
    {ok, Value};
integer(Value, [], _, _PrivArgs) when is_integer(Value) ->
    {ok, Value};
integer(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case binary_to_int(Value) of
        error -> {error, ?NOT_INTEGER};
        {ok, IntValue} -> {ok, IntValue}
    end;
integer(Value, [], _, _PrivArgs) when is_float(Value) ->
    {error, ?NOT_INTEGER};
integer(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec positive_integer(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
positive_integer(<<>> = Value, [], _, _PrivArgs) ->
    {ok, Value};
positive_integer(Value, [], _, _PrivArgs) when is_integer(Value), Value > 0 ->
    {ok, Value};
positive_integer(Value, [], _, _PrivArgs) when is_integer(Value), Value =< 0 ->
    {error, ?NOT_POSITIVE_INTEGER};
positive_integer(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case binary_to_int(Value) of
        error -> {error, ?NOT_POSITIVE_INTEGER};
        {ok, IntValue} when IntValue > 0 -> {ok, IntValue};
        _ -> {error, ?NOT_POSITIVE_INTEGER}
    end;
positive_integer(Value, [], _, _PrivArgs) when is_float(Value) ->
    {error, ?NOT_POSITIVE_INTEGER};
positive_integer(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec decimal(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
decimal(<<>> = Value, [], _, _PrivArgs) ->
    {ok, Value};
decimal(Value, [], _, _PrivArgs) when is_float(Value) ->
    {ok, Value};
decimal(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case binary_to_flt(Value) of
        error -> {error, ?NOT_DECIMAL};
        {ok, FltValue} -> {ok, FltValue}
    end;
decimal(Value, [], _, _PrivArgs) when is_integer(Value) ->
    {error, ?NOT_DECIMAL};
decimal(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec positive_decimal(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
positive_decimal(<<>> = Value, [], _, _PrivArgs) ->
    {ok, Value};
positive_decimal(Value, [], _, _PrivArgs) when is_float(Value), Value > 0 ->
    {ok, Value};
positive_decimal(Value, [], _, _PrivArgs) when is_float(Value), Value =< 0 ->
    {error, ?NOT_POSITIVE_DECIMAL};
positive_decimal(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case binary_to_flt(Value) of
        error -> {error, ?NOT_POSITIVE_DECIMAL};
        {ok, FltValue} when FltValue > 0 -> {ok, FltValue};
        _ -> {error, ?NOT_POSITIVE_DECIMAL}
    end;
positive_decimal(Value, [], _, _PrivArgs) when is_integer(Value) ->
    {error, ?NOT_POSITIVE_DECIMAL};
positive_decimal(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec max_number(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
max_number(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
max_number(Value, [MaxNumber], AllData, PrivArgs) ->
    max_number(Value, MaxNumber, AllData, PrivArgs);
max_number(Value, MaxNumber, _, _PrivArgs) when is_number(Value), Value =< MaxNumber ->
    {ok, Value};
max_number(Value, _Args, _, _PrivArgs) when is_number(Value) ->
    {error, ?TOO_HIGH};
max_number(Value, MaxNumber, _, _PrivArgs) when is_binary(Value) ->
    IntRes = binary_to_int(Value),
    FltRes = binary_to_flt(Value),
    case {IntRes, FltRes} of
        {error, error} -> {error, ?NOT_NUMBER};
        {{ok, IntValue}, _} when IntValue =< MaxNumber -> {ok, IntValue};
        {_, {ok, FltValue}} when FltValue =< MaxNumber -> {ok, FltValue};
        _ -> {error, ?TOO_HIGH}
    end;
max_number(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec min_number(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
min_number(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
min_number(Value, [MinNumber], AllData, _PrivArgs) ->
    min_number(Value, MinNumber, AllData, _PrivArgs);
min_number(Value, MinNumber, _, _PrivArgs) when is_number(Value), Value >= MinNumber ->
    {ok, Value};
min_number(Value, _Args, _, _PrivArgs) when is_number(Value) ->
    {error, ?TOO_LOW};
min_number(Value, MinNumber, _, _PrivArgs) when is_binary(Value) ->
    IntRes = binary_to_int(Value),
    FltRes = binary_to_flt(Value),
    case {IntRes, FltRes} of
        {error, error} -> {error, ?NOT_NUMBER};
        {{ok, IntValue}, _} when IntValue >= MinNumber -> {ok, IntValue};
        {_, {ok, FltValue}} when FltValue >= MinNumber -> {ok, FltValue};
        _ -> {error, ?TOO_LOW}
    end;
min_number(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec number_between(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
number_between(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
number_between(Value, [[MinNumber, MaxNumber]], AllData, _PrivArgs) ->
    number_between(Value, [MinNumber, MaxNumber], AllData, _PrivArgs);
number_between(Value, [MinNumber, MaxNumber], _, _PrivArgs)
    when is_number(Value), Value >= MinNumber, Value =< MaxNumber ->
    {ok, Value};
number_between(Value, [MinNumber, _], _, _PrivArgs) when is_number(Value), Value < MinNumber ->
    {error, ?TOO_LOW};
number_between(Value, [_, MaxNumber], _, _PrivArgs) when is_number(Value), Value > MaxNumber ->
    {error, ?TOO_HIGH};
number_between(Value, [MinNumber, MaxNumber], _, _PrivArgs) when is_binary(Value) ->
    IntRes = binary_to_int(Value),
    FltRes = binary_to_flt(Value),
    case {IntRes, FltRes} of
        {error, error} -> {error, ?NOT_NUMBER};
        {{ok, IntValue}, _} when IntValue > MaxNumber -> {error, ?TOO_HIGH};
        {{ok, IntValue}, _} when IntValue < MinNumber -> {error, ?TOO_LOW};
        {_, {ok, FltValue}} when FltValue > MaxNumber -> {error, ?TOO_HIGH};
        {_, {ok, FltValue}} when FltValue < MinNumber -> {error, ?TOO_LOW};
        {{ok, IntValue}, _} -> {ok, IntValue};
        {_, {ok, FltValue}} -> {ok, FltValue}
    end;
number_between(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

%% INTERNAL
binary_to_int(Value) ->
    try
        IntValue = binary_to_integer(Value),
        {ok, IntValue}
    catch
        _:_ -> error
    end.

%% TODO REFACTOR!
binary_to_flt(Value) ->
    case binary:match(Value, <<",">>) of
        nomatch ->
            try
                FltValue = binary_to_float(Value),
                {ok, FltValue}
            catch
                _:_ ->
                    try
                        FltValue1 = binary_to_integer(Value),
                        {ok, FltValue1}
                    catch
                        _:_ -> error
                    end
            end;
        _ -> error
    end.
