-module(olifer_special).
-author("prots.igor@gmail.com").

-include("olifer.hrl").

%% API
-export([email/4]).
-export([url/4]).
-export([iso_date/4]).
-export([equal_to_field/4]).

-spec email(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
email(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
email(Value, [], _, _PrivArgs) when is_binary(Value) ->
    Pattern = "^((?:(?:[^\"@\\.\s]+\\.?)|(?:\\.\"[^\"\s]+\"\\.))*(?:(?:\\.?\"[^\"\s]+\")|(?:[a-zA-Z0-9\\-_]+)))@[a-z0-9\\.\\-\\[\\]]+$",
    case re:run(Value, Pattern, [caseless, anchored]) of
        nomatch -> {error, ?WRONG_EMAIL};
        _ -> {ok, Value}
    end;
email(_, _, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

%%TODO not validate URLs with special symbols, like '_'
-spec url(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
url(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
url(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case http_uri:parse(binary_to_list(Value)) of
        {ok, ParsedUrl} -> check_url_details(ParsedUrl, Value);
        {error, _} -> {error, ?WRONG_URL}
    end;
url(_, _, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec iso_date(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
iso_date(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
iso_date(Value, [], _, _PrivArgs) when is_binary(Value) ->
    case binary:split(Value, <<"-">>, [global]) of
        [_, _, _] = DateBin -> check_date(DateBin, Value);
        _ -> {error, ?WRONG_DATE}
    end;
iso_date(_, _, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.

-spec equal_to_field(binary() | list(), [any()], [any()], [any()]) ->
    {ok, binary()} | {error, binary()}.
equal_to_field(<<>> = Value, _Args, _, _PrivArgs) ->
    {ok, Value};
equal_to_field(Value, [FieldName], AllData, PrivArgs) when is_binary(Value); is_integer(Value); is_float(Value) ->
    equal_to_field(Value, FieldName, AllData, PrivArgs);
equal_to_field(Value, FieldName, AllData, _PrivArgs) when is_binary(Value); is_integer(Value); is_float(Value) ->
    FieldValue = olifer:get_value(FieldName, AllData),
    case Value =:= FieldValue of
        true -> {ok, Value};
        false -> {error, ?FIELDS_NOT_EQUAL}
    end;
equal_to_field(_Value, _Args, _, _PrivArgs) ->
    {error, ?FORMAT_ERROR}.


%% INTERNAL
check_url_details({http, _UserInfo, Host, _Port, _Path, _Query}, Value) ->
    check_hostname(Host, Value);
check_url_details({https, _UserInfo, Host, _Port, _Path, _Query}, Value) ->
    check_hostname(Host, Value);
check_url_details(_, _) ->
    {error, ?WRONG_URL}.

check_hostname(Host, Value) ->
    Pattern = "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$",
    case re:run(Host, Pattern, [caseless, {capture, none}]) of
        nomatch -> {error, ?WRONG_URL};
        _ -> {ok, Value}
    end.

check_date(DateBin, Value) ->
    case [binary_to_int(Bin)|| Bin <- DateBin] of
        [{ok, Year}, {ok, Month}, {ok, Day}] ->
            case calendar:valid_date({Year, Month, Day}) of
                true -> {ok, Value};
                _ -> {error, ?WRONG_DATE}
            end;
        _ ->
            {error, ?WRONG_DATE}
    end.

binary_to_int(Value) ->
    try
        IntValue = binary_to_integer(Value),
        {ok, IntValue}
    catch
        _:_ -> error
    end.

